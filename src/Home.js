import React, {useRef} from "react";
import './Home.css';
import Shop from "./pages/shop/Shop";
import { User, IdentificationBadge, EnvelopeSimple, LockSimple } from "phosphor-react";

function Home(){
    const name = useRef()
    const email = useRef()
    const password = useRef()
    const getName=localStorage.getItem("nameData")
    const getEmail=localStorage.getItem("emailData")
    const getPassword=localStorage.getItem("passwordData")

    const handleSubmit=()=>{
        if(name.current.value==="Rudra"&&email.current.value==="abc@gmail.com"&&password.current.value==="12345"){
            localStorage.setItem("nameData","Rudra")
            localStorage.setItem("emailData","abc@gmail.com")
            localStorage.setItem("passwordData","12345")
    }}

    return(
        <div className="Login">
            {
                getName&&getEmail&&getPassword?
                <Shop />:
            <form onSubmit={handleSubmit}>
            <div className="main2">
                <div className="sub-main">
                    <div>
                        <div className="imgs">
                            <div className="container-image">
                                <User size={32} />
                            </div>
                        </div>
                        <div>
                            <h1>Login Page</h1>
                            <div>
                                <IdentificationBadge size={32}/>
                                <input type="text" placeholder="username" className="name" ref={name}/>
                            </div>
                            <div className="second-input">
                                <EnvelopeSimple size={32}/>
                                <input type="email" placeholder="email" className="name" ref={email}/>
                            </div>
                            <div className="second-input">
                                <LockSimple size={32}/>
                                <input type="password" placeholder="password" className="name" ref={password}/>
                            </div>
                            <div className="login-button">
                                <button className="button">Login</button>
                            </div> 
                            <p className="link">
                            <a href="#">Forgot password?</a> Or <a href="#">Sign Up</a>
                            </p>                
                        </div>
                    </div>
                </div>
            </div>
            </form>
            }
        </div>
    )
}

export default Home;