import React, {useState, useContext, useEffect} from "react";
import './shop.css';
import data from "../../Data/product.json"
import { ShopContext } from "../../context/shop-context";

function Shop(){
    const [searchTerm, setSearchTerm] = useState("");
    const {addToCart} = useContext(ShopContext);

    useEffect(() => {
      localStorage.setItem("Cart1", JSON.stringify(data));
    }, [data]
    );
  return (
    <>
      <div className="templateContainer">
        <div className="shoptitle">
        <h1>Thrift Shop</h1>
        </div>
        <div className="searchInput_Container">
          <input id="searchInput" type="text" placeholder="Search here..." onChange={(event) => {
            setSearchTerm(event.target.value);
          }} />
        </div>
        <div className="template_Container">
          {
            data 
              .filter((val) => {
                if(searchTerm === ""){
                  return val;
                }else if(val.productName.toLowerCase().includes(searchTerm.toLowerCase())){
                  return val;
                }
              })
              .map((val) => {
                return(
                  <div className="template" key={val.id}>
                      <img src={val.image}/>
                      <h3>{val.productName}</h3>
                      <p className="price">₹{val.price}</p>
                      <button className="addtocart" onClick={() => {addToCart(val.id); alert("Your item added to cart"); }}>
                        Add to Cart</button>
                  </div> 
                )
              })
          }
        </div>
      </div>
    </>
  )
}

export default Shop;