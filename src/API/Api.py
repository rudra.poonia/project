from flask import Flask, jsonify,request

app=Flask(__name__)

products = [
    {
        "id": 1,
        "productName": "Flag",
        "price": 999.0,
        "image": "https://thumbs.dreamstime.com/b/winning-flag-6771391.jpg"
    },
    {
        "id": 2,
        "productName": "Dice",
        "price": 1999.0,
        "image": "https://assets.ajio.com/medias/sys_master/root/20210811/eucw/6112e1d7f997ddce899ac03e/-1117Wx1400H-4919022660-multi-MODEL.jpg"
    },
    {
        "id": 3,
        "productName": "Dog",
        "price": 699.0,
        "image": "https://cdn.britannica.com/16/234216-050-C66F8665/beagle-hound-dog.jpg"
    },
    {
        "id": 4,
        "productName": "Simpson",
        "price": 228.0,
        "image": "https://upload.wikimedia.org/wikipedia/en/a/aa/Bart_Simpson_200px.png"
    },
    {
        "id": 5,
        "productName": "Sun Image",
        "price": 19.0,
        "image": "https://easydrawingguides.com/wp-content/uploads/2017/06/how-to-draw-a-sun-featured-image-1200.png"
    },
    {
        "id": 6,
        "productName": "Mountain",
        "price": 68.0,
        "image": "https://cdn.cdnparenting.com/articles/2018/08/602444213-H-1024x700.webp"
    },
    {
        "id": 7,
        "productName": "Surfing",
        "price": 120.0,
        "image": "https://www.daysoftheyear.com/wp-content/uploads/surf-day.jpg"
    },
    {
        "id": 8,
        "productName": "Lake",
        "price": 40.0,
        "image": "https://cdn.britannica.com/97/158797-050-ABECB32F/North-Cascades-National-Park-Lake-Ann-park.jpg"
    }
]

@app.route('/',methods=['GET'])

def get_products():
    return jsonify(products)

if __name__=="__main__":
    app.run(debug=True)