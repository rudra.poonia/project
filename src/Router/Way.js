import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {Navbar} from '../components/navbar';
import {Cart} from "../pages/cart/cart";
import Shop from '../pages/shop/Shop';
import { ShopContextProvider } from "../context/shop-context";
import Home from '../Home';

export default function Way(){
    return(
        <div>
        <ShopContextProvider>
        <Router>
        <Navbar/>
            <Routes>
                <Route path="/" element={<Home />}></Route>
                <Route path="/shop" element={<Shop />}></Route>
                <Route path="/cart" element={<Cart />} /> 
                {/*<Route path="/contact" element={<Contact />} />*/}
            </Routes>
        </Router>
        </ShopContextProvider>
        </div>
    )
}