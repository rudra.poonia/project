import React from "react";
import { Link } from "react-router-dom";
import { User, SignOut, ShoppingCart, UserRectangle } from "phosphor-react";
import "./navbar.css";

export const Navbar = () => {

  const handleClick = () => {
    localStorage.clear();
    window.location.reload();
  }

  return (
    <div className="navbar">
      <div className="links">
        <button onClick={handleClick}><SignOut size={32} /></button>
        <User size={32} color="white" />
        <Link to="/shop"> Shop </Link>
        <Link to="/contact"> Contact </Link>
        <Link to="/cart">
          <ShoppingCart size={32} />
        </Link>
      </div>
    </div>
  );
};